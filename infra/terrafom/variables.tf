variable "project" {
  description = "Project ID"
}

variable "region" {
  description = "default region"
  default     = "us-central1"
}
