variable "project" {
  description = "Project ID"
}

variable "region" {
  description = "default region"
  default     = "us-central1"
}

variable "zone" {
  description = "default zone"
  default     = "us-central1-a"
}


variable "public_key_path" {
  description = "Path to the public key used for ssh access"
}

variable "public_key_path_userubuntu" {
  description = "Path to the public key used for ssh access user ubuntu"
}

variable "private_key" {
  description = "Path to the private key used for ssh access"
}

variable "disk_image" {
  description = "Disk image"
}

variable "app_disk_image" {
  description = "Disk image for reddit app"
  default     = "reddit-base-app"
}

variable "db_disk_image" {
  description = "Disk image for reddit db"
  default     = "reddit-base-db"
}