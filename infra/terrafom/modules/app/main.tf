resource "google_compute_address" "app_external_ip" { name = "reddit-app-ip" }

resource "google_compute_instance" "app" {
  name         = "reddit-app"
  machine_type = "e2-small"
  zone         = var.zone
  tags         = ["reddit-app"]
  boot_disk {
    initialize_params {
      image = var.app_disk_image
      labels = {
        my_label = "value"
      }
    }
  }
  #Provisioning model
  scheduling {
    preemptible                 = true
    automatic_restart           = false
    provisioning_model          = "SPOT"
    instance_termination_action = "STOP"
  }
  network_interface {
    network = "default"
    access_config {
    nat_ip  = "${google_compute_address.app_external_ip.address}"
    }
  }
   metadata = {
    ssh-keys = "appuser:${file(var.public_key_path)}"
  }
  
}
