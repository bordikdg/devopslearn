variable "zone" {
  description = "default zone"
  default     = "us-central1-a"
}

variable db_disk_image {
  description = "Disk image for reddit db"
  default = "reddit-base-db"
}

variable "public_key_path" {
  description = "Path to the public key used for ssh access"
  default = "~/.ssh/appuser.pub"
}