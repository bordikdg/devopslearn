resource "google_compute_instance" "db" {
  name         = "reddit-db"
  machine_type = "e2-small"
  zone         = var.zone
  tags         = ["reddit-db"]
  boot_disk {
    initialize_params {
      image = var.db_disk_image
    }
  }

  network_interface {
    network         = "default"
    access_config {
      // Ephemeral public IP
    }

  }
  metadata = {
    ssh-keys = "appuser:${file(var.public_key_path)}"
  }
  
  #Provisioning model
  scheduling {
    preemptible                 = true
    automatic_restart           = false
    provisioning_model          = "SPOT"
    instance_termination_action = "STOP"
  }
}