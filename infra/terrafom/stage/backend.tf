terraform {
  backend "gcs" {
    bucket  = "tf-state-prod-stage"
    prefix  = "terraform/state"
  }
}