terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.51.0"
    }
  }
}


provider "google" {
  project = var.project
  region  = var.region
}

module "app" {
  source          = "../modules/app"
  public_key_path = var.public_key_path
  zone            = var.zone
  app_disk_image  = var.app_disk_image
}
module "db" {
  source          = "../modules/db"
  public_key_path = var.public_key_path
  zone            = var.zone
  db_disk_image   = var.db_disk_image
}

module "vpc" {
  source        = "../modules/vpc"
  source_ranges = "0.0.0.0/0"
}

resource "google_compute_project_metadata" "default" {
  metadata = {
    "ssh-keys" = <<-EOT
    appuser:${file(var.public_key_path)}
    ubuntu:${file(var.public_key_path_userubuntu)}
      EOT
  }
}
