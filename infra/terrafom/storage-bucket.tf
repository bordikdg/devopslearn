provider "google" {
  project = var.project
  region  = var.region
}
module "awesome_bucket" {
  source   = "git::https://github.com/SweetOps/terraform-google-storage-bucket.git?ref=master"
  name     = "tf-state-prod-stage"
  location = var.region
  enabled  = "1"
}

