#!/bin/bash 
gcloud compute instances create reddit-app01 \
  --image-family reddit-full \
  --tags puma-server \
  --machine-type=e2-small \
  --restart-on-failure 

